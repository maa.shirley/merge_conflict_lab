public class Rectangle {
 private double length;
 private double width;
 public Rectangle(double width, double length){
    this.width=width; 
    this.length=length; 
 }   
 public double getwidth(){
     return this.width;
 }
 public double getLength(){
     return this.length;
 }
 public double getArea(){
     return this.width*this.length;
 }
 public String toString(){
     return "The area is " + getArea();
 }
}
